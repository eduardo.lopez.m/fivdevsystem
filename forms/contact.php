<?php
  /**
  * Requires the "PHP Email Form" library
  * The "PHP Email Form" library is available only in the pro version of the template
  * The library should be uploaded to: vendor/php-email-form/php-email-form.php
  * For more info and help: https://bootstrapmade.com/php-email-form/
  */

  try {
    $headers = "MIME-Version: 1.0\r\n"; 
    // $headers .= "Content-type: text/html; charset=iso-8859-1\r\n"; 
    $headers .= "Content-Type: text/html; charset=UTF-8";
    $headers .= "From: ".($_POST['name'])." <".$_POST['email'].">\r\n";

    $correoDestino = "comercial@fivdevsystem.com";

    $subject = $_POST['subject'];
    $message = $_POST['message'];

    //dirección de respuesta, si queremos que sea distinta que la del remitente 
    $headers .= "Reply-To: ".$_POST['email']."\r\n";
    if (mail($correoDestino, ($subject), ($message), $headers)) {
      echo "Información enviada!";
    } else {
      echo "Ocurrió un problema al enviar la información, contactar al teléfono (669) 213-1875";
    }
  } catch (Exception $e) {
    echo "Ocurrió un problema al enviar la información, contactar al teléfono (669) 213-1875";
  }

  exit();

  // Replace contact@example.com with your real receiving email address
  $receiving_email_address = 'contact@example.com';

  if( file_exists($php_email_form = '../assets/vendor/php-email-form/php-email-form.php' )) {
    include( $php_email_form );
  } else {
    die( 'Unable to load the "PHP Email Form" Library!');
  }

  $contact = new PHP_Email_Form;
  $contact->ajax = true;
  
  $contact->to = $receiving_email_address;
  $contact->from_name = $_POST['name'];
  $contact->from_email = $_POST['email'];
  $contact->subject = $_POST['subject'];

  // Uncomment below code if you want to use SMTP to send emails. You need to enter your correct SMTP credentials
  /*
  $contact->smtp = array(
    'host' => 'example.com',
    'username' => 'example',
    'password' => 'pass',
    'port' => '587'
  );
  */

  $contact->add_message( $_POST['name'], 'From');
  $contact->add_message( $_POST['email'], 'Email');
  $contact->add_message( $_POST['message'], 'Message', 10);

  echo $contact->send();
?>